package ivanhoe.client;

import ivanhoe.util.Config;
import ivanhoe.view.ViewFactory;
import ivanhoe.view.ViewFactoryImpl;

public class StartClient {
	
	public static void main(String[] argv) {
		ViewFactory viewFactory = new ViewFactoryImpl();
		AppClient client = new AppClient(viewFactory, Config.DEFAULT_SERVER_ADDRESS, Config.DEFAULT_SERVER_PORT); 
		//boolean success = client.connect();
	}
}

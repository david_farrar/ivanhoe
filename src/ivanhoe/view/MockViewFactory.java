package ivanhoe.view;

import ivanhoe.client.AppClient;

public class MockViewFactory implements ViewFactory {
	
	public View createView(AppClient client) {
		return new MockView();
	}
}

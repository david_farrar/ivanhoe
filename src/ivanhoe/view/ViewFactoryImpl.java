package ivanhoe.view;

import ivanhoe.client.AppClient;
import ivanhoe.util.ClientRequestBuilder;

public class ViewFactoryImpl implements ViewFactory {
	public View createView(AppClient client) {
		return new TextViewImpl(client, new ClientRequestBuilder());
	}
}

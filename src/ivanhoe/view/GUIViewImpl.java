package ivanhoe.view;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.json.simple.JSONObject;

import ivanhoe.GUI.ChooseColorView;
import ivanhoe.GUI.FivePlayerTournament;
import ivanhoe.GUI.FourPlayerTournament;
import ivanhoe.GUI.IvanhoeFrame;
import ivanhoe.GUI.StartView;
import ivanhoe.GUI.ThreePlayerTournament;
import ivanhoe.GUI.TournamentView;
import ivanhoe.GUI.TwoPlayerTournament;
import ivanhoe.GUI.WaitingView;
import ivanhoe.client.AppClient;
import ivanhoe.util.ClientParser;
import ivanhoe.util.ClientRequestBuilder;

public class GUIViewImpl implements View{	
	public AppClient client;
	public ClientRequestBuilder requestBuilder;
	public boolean running = false;
	String username;
	
	IvanhoeFrame currentWindow;
	
	JSONObject snapshot;
	
	public GUIViewImpl(AppClient client, ClientRequestBuilder requestBuilder, JSONObject snapshot){
		this.client = client;
		this.requestBuilder = requestBuilder;
		this.snapshot = snapshot;
		this.username = client.getUsername();
	}
	
	@Override
	public void displayTournamentView(JSONObject snapshot) {
		if(currentWindow!=null)
			currentWindow.dispose();

		int players = ClientParser.getPlayerList(snapshot).size(); // replace with num players
		if(players == 2)
			currentWindow = new TwoPlayerTournament(this,snapshot);
		if(players == 3)
			currentWindow = new ThreePlayerTournament(this,snapshot);
		else if(players == 4)
			currentWindow = new FourPlayerTournament(this,snapshot);
		else
			currentWindow = new FivePlayerTournament(this,snapshot);
	}
	
	public void displayWaitingMessage() {
		if(currentWindow!=null)
			currentWindow.dispose();
		currentWindow = new WaitingView(this);
	}
	
	
	public void launch() {
		running = true;
		displayWelcome();
	}

	public void displayStartScreen(JSONObject snapshot) {
		if(currentWindow!=null)
			currentWindow.dispose();
		currentWindow = new StartView(this,snapshot);
	}
	
	
	public void displayChooseColor() {
		System.out.println("Choose the color of the next tournament: ");
		if(currentWindow!=null)
			currentWindow.dispose();
		currentWindow = new ChooseColorView();
	}
	
	public void displayTurnView() {
			((TournamentView)currentWindow).setActiveTurn(true);
	}


	public void stop() {
		currentWindow.dispose();
		running = false;
	}
	
	public void chooseColor(){
		//JSONObject request = requestBuilder.buildChooseToken();
		//client.handleClientRequest(request);
	}
	
	public void connect(String username){
		client.setUsername(username);
		client.connect();
	}

	@Override
	public void displayInvalidMove() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayTurnView(String drawnCard) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayTurnPlayer(String playerName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void announceWithdrawal(String string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayTournamentWonMessage(String tokenColor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayTournamentLossMessage(String winnerName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayGameWonMessage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayGameLossMessage(String string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayPurpleTournamentWonMessage() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void displayChooseToken(JSONObject server_response) {
		// TODO Auto-generated method stub
		
	}
	
	public void withdraw(){
		JSONObject request = requestBuilder.buildWithdrawMove();
		try {
			client.handleClientRequest(request);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(currentWindow, "Failed Move or Action");
		}
	}
	
	public void selectCard(String player, String card){
		JSONObject request = null;
		
		try {
			client.handleClientRequest(request);
			((TournamentView)currentWindow).setActiveTurn(false);
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(currentWindow, "Failed Move or Action");
		}
	}
	
	@Override
	public void displayWelcome() {
		// TODO Auto-generated method stub
		
	}
	
	public String getUsername(){
		return username;
	}



}

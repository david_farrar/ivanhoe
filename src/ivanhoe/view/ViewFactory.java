package ivanhoe.view;

import ivanhoe.client.AppClient;

public interface ViewFactory {
	public View createView(AppClient client);
}

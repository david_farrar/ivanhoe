package ivanhoe.controller;

import java.util.ArrayList;
import java.util.HashMap;

import ivanhoe.model.Card;
import ivanhoe.model.Player;
import ivanhoe.model.Token;
import ivanhoe.model.Tournament;
import ivanhoe.server.AppServer;
import ivanhoe.util.ServerResponseBuilder;

/**
 * Extension of controller with testing capabilities. Mostly contains getters and setters
 * to data members you typically wouldn't want accessed, but that are necessary for setting
 * up test cases
 * @author PJF
 *
 */
public class MockController extends IvanhoeController {

	public MockController(AppServer server, ServerResponseBuilder responseBuilder, int maxPlayers) {
		super(server, responseBuilder, maxPlayers);
	}
	
	public Tournament getTournament() { return currentTournament; }
	public int getCurrentTurn() { return currentTurn; }
	
	public void setPlayers(HashMap<Integer, Player> players) {
		this.players = players;
	}
	
	public void setTournament(Tournament tournament) {
		this.currentTournament = tournament;
	}
	
	public void setPreviousTournament(Token color) {
		this.previousTournament = color;
	}
	
	public Token getPreviousTournament() {
		return previousTournament;
	}
	
	public void givePlayerToken(int playerId, Token token) {
		players.get(playerId).addToken(token);
	}
	
	public void setTurn(int playerId) {
		
		if (playerTurns == null) {
			playerTurns = new ArrayList<Integer>(players.keySet());
		}
		
		for (int i = 0; i < playerTurns.size(); ++i) {
			if (playerTurns.get(i) == playerId) {
				currentTurn = i;
			}
		}
		
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	public int getState() {
		return state;
	}
	
	public Card getCardFromDeck(String cardCode) {
		for (Card c: currentTournament.getDeck()) {
			if (c.toString().equals(cardCode)) {
				return c;
			}
		}
		return null;
	}
	
	public ArrayList<Integer> getPlayerTurns() {
		return playerTurns;
	}
	
	public void setPlayerTurns(ArrayList<Integer> turns) {
		this.playerTurns = turns;
	}
	
	public HashMap<Integer, Player> getPlayers() { 
		for (Integer key: players.keySet()) {
			System.out.println(key + ": " + players.get(key));
		}
		return players;
	}

	public Object getCardsFromStrings(String[] alexeiCards) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPlayerTurns(Integer[] turnSequence) {
		// TODO Auto-generated method stub
		
	}

	public void swapHand(int alexeiId, Object cardsFromStrings) {
		// TODO Auto-generated method stub
		
	}
	
}

package ivanhoe.util;

import java.io.*;
import java.util.Properties;

public class Config {
	public static final String DEFAULT_SERVER_ADDRESS = "127.0.0.1";
	public static final int DEFAULT_SERVER_PORT = 10000;
	
	public enum RequestType {
		REGISTER_PLAYER, MAKE_MOVE, QUIT;
	}

	public enum ResponseType {
		CONNECTION_ACCEPTED, CONNECTION_REJECTED,
		UPDATE_VIEW, START_PLAYER_TURN, START_GAME,
		MAKE_MOVE, QUIT;
	}
	
	public static boolean user_initialized = false;
	public static String user_server_address = "";
	public static int user_server_port = 0;
	public static String user_name = "";;
	public static int resolutionX = 1000;
	public static int resolutionY = 600;
	
	
	
	public static void save(){
		Properties prop = new Properties();
		OutputStream output = null;
		
		try {

			output = new FileOutputStream("config.ini");

			// set the properties value
			prop.setProperty("user_initialized", user_initialized+"");
			prop.setProperty("user_server_address", user_server_address);
			prop.setProperty("user_server_port", user_server_port+"");
			prop.setProperty("user_name", user_name);
			prop.setProperty("resolutionX", resolutionX+"");
			prop.setProperty("resolutionY", resolutionY+"");

			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	  }
	
	
	public static void load(){
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.ini");

			// load a properties file
			prop.load(input);
			
			user_initialized = Boolean.parseBoolean(prop.getProperty("user_initialized"));
			user_server_address = prop.getProperty("user_server_address");
			user_server_port = Integer.parseInt(prop.getProperty("user_server_port"));
			user_name = prop.getProperty("user_name");
			resolutionX = Integer.parseInt(prop.getProperty("resolutionX"));
			resolutionY = Integer.parseInt(prop.getProperty("resolutionY"));
			

		} catch (IOException ex) {
			save();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}


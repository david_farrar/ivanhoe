package ivanhoe.GUI;


/**
 * Typical display shown during a tournament. Shows player details (including hand,
 * tokens, etc.) as well as each of their opponent's displays. 
 * @author PJF
 *
 */
public interface TournamentView{
	public void withdrawAction();
	public void ivanhoeAction();
	public void cardAction(String card);
	public void cardDisplay(String card);
	public void setActiveTurn(boolean isActive);
}

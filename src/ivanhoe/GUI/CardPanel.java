package ivanhoe.GUI;

import java.util.ArrayList;

import javax.swing.JPanel;

public class CardPanel extends JPanel {
	private ArrayList<CardButton> cards;
	private TournamentView display;
	private String alignment;
	
	
	public CardPanel(TournamentView display,ArrayList<String> cards, String alignment, int x, int y, int width, int height){
		super();
		this.display = display;
		this.alignment = alignment;
		setBounds(x,y,width,height);
		refresh(cards);
	}
	
	/** obsolete
	public void addCard(String name){
		CardButton card = new CardButton(name);
		cards.add(card);
		add(card);
	}
	
	public Boolean removeCard(String name){
		for(CardButton c: cards){
			if(c.getName() == name){
				cards.remove(c);
				c.setVisible(false);
			}
		}
		return false;
	}
	*/
	
	public void refresh(ArrayList<String> cardNames){
		int i;
		for(i = 0; i<cards.size(); i++){
			if(cards.get(i).getName()!=cardNames.get(i)){
				cards.set(i, new CardButton(display, cardNames.get(i),0,0, false));
			}
		}
		if(cardNames.size()>i){
			for(i=i; i<cardNames.size();i++){
				cards.add(i, new CardButton(display, cardNames.get(i),0,0,false));
			}
		}
	}
}

package ivanhoe.GUI;

import javax.swing.*;

import org.json.simple.JSONObject;

import ivanhoe.util.Config;
import ivanhoe.view.GUIViewImpl;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Display shown when the app is launched; allows client to connect
 * to server and try to join in the Ivanhoe game
 * @author David Farrar
 *
 */
@SuppressWarnings("serial")
public class WelcomeView extends IvanhoeFrame{
	private GUIViewImpl view;
	
	private JLabel title, authors;
	private JLabel leftGraphic, rightGraphic;
	private JButton joinServer, startServer, config, exit;
	
	private int resX = Config.resolutionX;
	private int resY = Config.resolutionY; 
	
	public WelcomeView(GUIViewImpl v){
		this.view = v;
		Config.load();
		
		setSize(resX,resY+30);
		setLayout(null);
		setResizable(false);
		
		Image background = Toolkit.getDefaultToolkit().createImage("jpg/backgrounds/background_green.jpg");
		setContentPane(new ImagePanel(background));
		
		Image titleGraphic = Toolkit.getDefaultToolkit().createImage("jpg/ivanhoetitle.gif");
		titleGraphic = titleGraphic.getScaledInstance(500,170, java.awt.Image.SCALE_REPLICATE);
		JLabel title = new JLabel(new ImageIcon(titleGraphic));
		title.setBounds(resX/2-250, 0, 500, 170);
		title.setVisible(true);
		add(title);
		
		Image authorGraphic = Toolkit.getDefaultToolkit().createImage("jpg/author.gif");
		authorGraphic = authorGraphic.getScaledInstance(250,100, java.awt.Image.SCALE_REPLICATE);
		JLabel author = new JLabel(new ImageIcon(authorGraphic));
		author.setBounds(resX/2-125, 180, 250, 100);
		author.setVisible(true);
		add(author);
		
		Image joinGraphic = Toolkit.getDefaultToolkit().createImage("jpg/welcome/joinserver.gif");
		joinGraphic = joinGraphic.getScaledInstance(250,100, java.awt.Image.SCALE_REPLICATE);
		joinServer = new JButton();
		joinServer.setIcon(new ImageIcon(joinGraphic));
		joinServer.setBounds(0,resY/2,resX/2,resY/4);
		joinServer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				joinServer();
			}
		});
		joinServer.setVisible(true);
		add(joinServer);
		
		Image startGraphic = Toolkit.getDefaultToolkit().createImage("jpg/welcome/startserver.gif");
		startGraphic = startGraphic.getScaledInstance(250,100, java.awt.Image.SCALE_REPLICATE);
		startServer = new JButton();
		startServer.setIcon(new ImageIcon(startGraphic));
		startServer.setBounds(0,resY-resY/4,resX/2,resY/4);
		startServer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				startServer();
			}
		});
		startServer.setVisible(true);
		add(startServer);
		
		Image configGraphic = Toolkit.getDefaultToolkit().createImage("jpg/welcome/config.gif");
		configGraphic = configGraphic.getScaledInstance(250,100, java.awt.Image.SCALE_REPLICATE);
		config = new JButton();
		config.setIcon(new ImageIcon(configGraphic));
		config.setBounds(resX/2,resY/2,resX/2,resY/4);
		config.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				config();
			}
		});
		config.setVisible(true);
		add(config);
		
		Image exitGraphic = Toolkit.getDefaultToolkit().createImage("jpg/welcome/exit.gif");
		exitGraphic = joinGraphic.getScaledInstance(250,100, java.awt.Image.SCALE_REPLICATE);
		exit = new JButton();
		exit.setIcon(new ImageIcon(joinGraphic));
		exit.setBounds(resX/2,resY-resY/4,resX/2,resY/4);
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});
		exit.setVisible(true);
		add(exit);

		
		
		Image swordsman = Toolkit.getDefaultToolkit().createImage("jpg/welcome/swordsman.gif");
		swordsman = swordsman.getScaledInstance(resY/3,resY/3, java.awt.Image.SCALE_REPLICATE);
		JLabel leftGraphic = new JLabel(new ImageIcon(swordsman));
		leftGraphic.setBounds(0, 0, resY/3, resY/3);
		leftGraphic.setVisible(true);
		add(leftGraphic);
		
		Image axeman = Toolkit.getDefaultToolkit().createImage("jpg/welcome/axeman.gif");
		axeman = axeman.getScaledInstance(resY/3,resY/3, java.awt.Image.SCALE_REPLICATE);
		JLabel rightGraphic = new JLabel(new ImageIcon(swordsman));
		rightGraphic.setBounds(resX-(resY/3), 0, resY/3, resY/3);
		rightGraphic.setVisible(true);
		add(rightGraphic);
		
		setVisible(true);
	}
	
	public void startServer(){
		
	}
	
	public void joinServer(){
		
	}
	
	public void config(){
		
	}
	
	public void exit(){
		this.dispose();
	}

	@Override
	public void refresh(JSONObject snapshot) {
		// Nothing to update
	}
}

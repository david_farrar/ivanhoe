package ivanhoe.GUI;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class CardButton extends JButton{
	public static final int LARGECARDHEIGHT = 400;
	public static final int SMALLCARDHEIGHT = 200;
	
	private boolean useLargeCard = false;
	private String code;
	private TournamentView display;
	
	public CardButton(TournamentView display, String code,int x, int y, boolean useLargeCard){
		super();
		
		this.useLargeCard = useLargeCard;
		this.display = display;
		this.code = code;
		
		setImage(code);
			
		setLocation(x,y);
		
		
		addMouseListener(new MouseListener(){
			public void mouseClicked(MouseEvent arg0) {
				// does nothing
			}

			public void mouseEntered(MouseEvent arg0) {
				mousedOver();
			}

			public void mouseExited(MouseEvent arg0) {
				// does nothing
			}

			public void mousePressed(MouseEvent arg0) {
				cardPressed();
			}

			public void mouseReleased(MouseEvent arg0) {
				// does nothing
			}
			
		});
	}
	
	public void setLocation(int x, int y){
		if(useLargeCard)
			setBounds(x,y, (int)(LARGECARDHEIGHT*0.7),LARGECARDHEIGHT);
		else
			setBounds(x,y, (int)(SMALLCARDHEIGHT*0.7),SMALLCARDHEIGHT);
	}
	

	
	public void setImage(String code){
		ImageIcon temp = getImageFromCode(code, useLargeCard);
		setIcon(temp);
		setDisabledIcon(temp);
	}
	
	public String getCode(){
		return code;
	}
	
	public void cardPressed(){
		display.cardAction(code);
	}
	
	public void mousedOver(){
		display.cardDisplay(code);
	}
	
	public static ImageIcon getImageFromCode(String code, Boolean useLargeCard){
		ImageIcon temp = new ImageIcon("jpg/cards/" + code + ".jpg");
		Image img;
		if(useLargeCard)
			img =  temp.getImage().getScaledInstance((int)(LARGECARDHEIGHT*0.7), LARGECARDHEIGHT, java.awt.Image.SCALE_SMOOTH);
		else
			img =  temp.getImage().getScaledInstance((int)(SMALLCARDHEIGHT*0.7), SMALLCARDHEIGHT, java.awt.Image.SCALE_SMOOTH);
		return new ImageIcon(img);
	}
}

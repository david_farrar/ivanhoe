package ivanhoe.GUI;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

import org.json.simple.JSONObject;

import ivanhoe.util.ClientParser;
import ivanhoe.view.GUIViewImpl;

@SuppressWarnings("serial")
public class TwoPlayerTournament extends IvanhoeFrame implements TournamentView{

	private GUIViewImpl view;
	
	private JButton withdraw;
	
	private CardPanel playerHand;
	private CardPanel playerDisplay, opponentDisplay;
	
	private DrawPile drawPile;
	
	private String username;
	private String opponent;
	
	public TwoPlayerTournament(GUIViewImpl view, JSONObject snapshot){
		this.view = view;
		this.username = view.getUsername();
		
		setSize(800,600);
		setBackground(Color.GREEN);
		setLayout(null);
		setResizable(false);
		
		withdraw = new JButton("Start");
		withdraw.setBounds(0,550,100,50);
		withdraw.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				withdrawAction();	
			}
		});
		withdraw.setVisible(true);
		add(withdraw);

		ArrayList<Object> players = ClientParser.getPlayerList(snapshot);
		if(players.get(0)==username){
			username = username;
			opponent = (String)players.get(1);
		}
		else{ 
			username = username;
			opponent = (String)players.get(0);
		}
		
		playerHand = new CardPanel(this,ClientParser.getPlayerHand(username),"h",100,500,600,100);
		playerHand.setVisible(true);
		add(playerHand);

		playerDisplay = new CardPanel(this,ClientParser.getPlayerDisplay(username),"h",100,400,600,100);
		playerDisplay.setVisible(true);
		add(playerDisplay);

		opponentDisplay = new CardPanel(this,ClientParser.getPlayerDisplay(opponent),"h",100,0,600,100);
		opponentDisplay.setVisible(true);
		add(opponentDisplay);
		
		drawPile = new DrawPile(this, ClientParser.getDeck(snapshot),350,250,100,100);
			
		setVisible(true);
	}
	
	public void selectCardAction(String player, String card){
			view.selectCard(player, card);
	}

	public void withdrawAction() {
		view.withdraw();
	}
	
	public void refresh(JSONObject snapshot) {
		playerHand.refresh(ClientParser.getPlayerHand(username));

		playerDisplay.refresh(ClientParser.getPlayerDisplay(username));

		opponentDisplay.refresh(ClientParser.getPlayerDisplay(opponent));
		
		drawPile.refresh(ClientParser.getDeck(snapshot));
	}
	
	public void setActiveTurn(boolean isActive){
		playerHand.setEnabled(isActive);
		
		drawPile.setEnabled(isActive);
	}


	@Override
	public void ivanhoeAction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cardAction(String card) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cardDisplay(String card) {
		// TODO Auto-generated method stub
		
	}
}

package ivanhoe.GUI;

import javax.swing.JFrame;

import org.json.simple.JSONObject;

public abstract class IvanhoeFrame extends JFrame{
	
	public abstract void refresh(JSONObject snapshot);
}

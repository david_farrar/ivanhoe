package ivanhoe.GUI;

/**
 * Display shown when it is the player's turn to make a move. Switches over
 * from tournament display, gives player options (including withdrawing, picking
 * a card, etc.)
 * @author PJF
 *
 */
public interface TurnView {
	//
}

README

--- STARTING THE GAME

Both the client and the server have been exported in executable jars. To run the server,
write:

java -jar Server.jar

From there, you will be prompted to write "start", and then enter the number of players
the server supports. This is intended to be between 2-5 

(NOTE: because these test UIs were meant to be for testing / temporary purposes, there is 
limited testing on them and it is very possible to enter input that will break them.
For example, starting a game with 6 players)

Once the server is started, start clients with:

java -jar Client.jar

This will launch a client without connecting. to connect write "start_connect <username>"
The client will then wait until the game starts.

The game starts when enough clients are connected. If the server was started with 3
players, 3 clients must be started and connected.

NOTE: Again, the test UI is not robust or user friendly as it wasn't really intended to
be used. If you write start_connect <space> it will probably break the program.

*** NOTE2 ***: log4j worked fine in Eclipse but had problems once the program was exported to 
jars. There will be error messages but you can ignore them. The program works fine.

--- PLAYING THE GAME

The game will typically prompt you for input, such as
make_move <card> -----> ex: make_move g1, make_move p5, make_move s2
make_move withdraw
make_move g1 g1 g1 -> plays 3 green cards
choose_color blue -> (choices are blue, green, red, yellow, and purple - spelled lowercase)

NOTE: not robust against typos, case sensitive

An invalid move will receive a response of 'invalid move' and the player must pick
another move. Multiple cards can be played, as long as they are all valid and the total
will cause the player to have the highest display

*** NOTE2 *** : although action cards appear to be options, in reality these are not yet implemented
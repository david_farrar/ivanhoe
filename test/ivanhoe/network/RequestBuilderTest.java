package ivanhoe.network;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ivanhoe.model.Card;
import ivanhoe.model.ColourCard;
import ivanhoe.model.SupporterCard;
import ivanhoe.model.Token;
import ivanhoe.model.Tournament;
import ivanhoe.util.ClientRequestBuilder;
import ivanhoe.util.ServerParser;

public class RequestBuilderTest {

	private ClientRequestBuilder requestBuilder = new ClientRequestBuilder();
	private JSONParser parser = new JSONParser();
	private ServerParser serverParser = new ServerParser();
	private Tournament tournament;
	
	@Before
	public void setUp() throws Exception {
		tournament = new Tournament();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBuildRegisterPlayer() throws ParseException {
		
		String testMoveString = requestBuilder.buildRegisterPlayer("Alexei").toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "register_player");
		assertEquals(testMove.get("username"), "Alexei");
		
		// TODO: test weird cases
	}
	
	@Test
	public void testBuildChooseToken() throws ParseException {
		
		String testMoveString = requestBuilder.buildChooseToken("red").toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "choose_token");
		assertEquals(testMove.get("token_color"), "red");
		Token t = Token.fromString((String)testMove.get("token_color"));
		assertEquals(t, Token.RED);
		
		testMoveString = requestBuilder.buildChooseToken("purple").toJSONString();
		testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "choose_token");
		assertEquals(testMove.get("token_color"), "purple");
		t = Token.fromString((String)testMove.get("token_color"));
		assertEquals(t, Token.PURPLE);
		
	}
	
	@Test
	public void testBuildChooseTokenInvalid() throws ParseException {
		String testMoveString = requestBuilder.buildChooseToken("foo").toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "choose_token");
		assertEquals(testMove.get("token_color"), "foo");
		Token t = Token.fromString((String)testMove.get("token_color"));
		assertNull(t);
	}
	
	@Test
	public void testBuildCardMove() throws ParseException {
		
		// test some color cards
		String testMoveString = requestBuilder.buildCardMove("r3").toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 3);
		assertEquals(testMove.get("request_type"), "turn_move"); 
		assertEquals(testMove.get("move_type"), "play_card");
		assertEquals(testMove.get("card_code"), "r3");
		
		ArrayList<Card> cards = serverParser.getCard(testMove, tournament);
		assertTrue(cards.get(0) instanceof ColourCard);
		assertEquals(cards.get(0).getName(), "red");
		assertEquals(cards.get(0).getValue(), 3);
		
		testMoveString = requestBuilder.buildCardMove("p5").toJSONString();
		testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 3);
		assertEquals(testMove.get("request_type"), "turn_move"); 
		assertEquals(testMove.get("move_type"), "play_card");
		assertEquals(testMove.get("card_code"), "p5");
		
		cards = serverParser.getCard(testMove, tournament);
		assertTrue(cards.get(0) instanceof ColourCard);
		assertEquals(cards.get(0).getName(), "purple");
		assertEquals(cards.get(0).getValue(), 5);
		
		// test some supporter cards
		testMoveString = requestBuilder.buildCardMove("s3").toJSONString();
		testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 3);
		assertEquals(testMove.get("request_type"), "turn_move"); 
		assertEquals(testMove.get("move_type"), "play_card");
		assertEquals(testMove.get("card_code"), "s3");
		
		cards = serverParser.getCard(testMove, tournament);
		assertTrue(cards.get(0) instanceof SupporterCard);
		assertEquals(cards.get(0).getName().toLowerCase(), "squire");
		assertEquals(cards.get(0).getValue(), 3);
		
		testMoveString = requestBuilder.buildCardMove("m6").toJSONString();
		testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 3);
		assertEquals(testMove.get("request_type"), "turn_move"); 
		assertEquals(testMove.get("move_type"), "play_card");
		assertEquals(testMove.get("card_code"), "m6");
		
		cards = serverParser.getCard(testMove, tournament);
		assertTrue(cards.get(0) instanceof SupporterCard);
		assertEquals(cards.get(0).getName().toLowerCase(), "maiden");
		assertEquals(cards.get(0).getValue(), 6);
		
	}
	
	@Test
	public void testBuildWithdrawMove() throws ParseException {
		
		String testMoveString = requestBuilder.buildWithdrawMove().toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "turn_move");
		assertEquals(testMove.get("move_type"), "withdraw");
		
	}
	
	@Test
	public void testBuildSelectOpponent() throws ParseException {
		
		String testMoveString = requestBuilder.buildSelectOpponent("Alexei").toJSONString();
		JSONObject testMove = (JSONObject)parser.parse(testMoveString);
		
		assertEquals(testMove.size(), 2);
		assertEquals(testMove.get("request_type"), "select_opponent");
		assertEquals(testMove.get("opponent_username"), "Alexei");
		
	}

}

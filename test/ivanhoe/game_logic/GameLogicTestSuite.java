package ivanhoe.game_logic;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   WinningGameTest.class,
   ColourCardTest.class,
   SupporterCardTest.class,
   ActionCardTest.class,
   WithdrawTest.class,
   StartTournamentTest.class,
   WinningTournamentTest.class
})

public class GameLogicTestSuite {   
} 
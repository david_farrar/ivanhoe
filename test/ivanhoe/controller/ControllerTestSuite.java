package ivanhoe.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   TurnTest.class,
})

public class ControllerTestSuite {   
} 
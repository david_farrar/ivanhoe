package ivanhoe.view.GUI;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.*;

import org.json.simple.JSONObject;

import ivanhoe.GUI.*;
import ivanhoe.util.Config;

public class JPEGTest extends JFrame implements TournamentView{
	
	CardButton displayedCard;
	
	public JPEGTest(){
		Config.load();
		setSize(Config.resolutionX,Config.resolutionY);
		
		setBackground(Color.GREEN);
		setLayout(null);
		setResizable(false);
		
		Image background = Toolkit.getDefaultToolkit().createImage("jpg/backgrounds/background_green.jpg");
		setContentPane(new ImagePanel(background));
		
		ArrayList<CardButton> cards = new ArrayList<CardButton>();
		
		String code = "";
		
		displayedCard = new CardButton(this,"cardBack",1000,600,true);
		displayedCard.setVisible(true);
		displayedCard.setEnabled(false);
		add(displayedCard);
		
		for(int i = 0; i<36; i++){
			switch(i){
				case 0: code = "adapt";
					break;
				case 1: code = "b2";
					break;
				case 2: code = "b3";
					break;
				case 3: code = "b4";
					break;
				case 4: code = "b5";
					break;
				case 5: code = "breaklance";
					break;
				case 6: code = "cardBack";
					break;
				case 7: code = "countercharge";
					break;
				case 8: code = "charge";
					break;
				case 9: code = "changeweapon";
					break;
				case 10:code = "dodge";
					break;
				case 11:code = "disgrace";
					break;
				case 12:code = "dropweapon";
					break;
				case 13:code = "ivanhoe";
					break;
				case 14:code = "knockdown";
					break;
				case 15:code = "outmaneuver";
					break;
				case 16:code = "outwit";
					break;
				case 17:code = "p3";
					break;
				case 18:code = "p4";
					break;
				case 19:code = "p5";
					break;
				case 20:code = "p7";
					break;
				case 21:code = "r3";
					break;
				case 22:code = "r4";
					break;
				case 23:code = "r5";
					break;
				case 24:code = "riposte";
					break;
				case 25:code = "retreat";
					break;
				case 26:code = "s2";
					break;
				case 27:code = "s3";
					break;
				case 28:code = "shield";
					break;
				case 29:code = "stunned";
					break;
				case 30:code = "unhorse";
					break;
				case 31:code = "y2";
					break;
				case 32:code = "y3";
					break;
				case 33:code = "y4";
					break;
				case 34:code = "m6";
					break;
				case 35:code = "g1";
					break;
			}
			CardButton temp = new CardButton(this, code,140*(i%11),(int)(i/11)*200,false);
			temp.setVisible(true);
			add(temp);
		}
		
		setVisible(true);
	}
	
	public void cardDisplay(String code) {
		displayedCard.setImage(code);
	}
	
	public static void main(String[] args){
		new JPEGTest();
	}



	@Override
	public void cardAction(String card) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setActiveTurn(boolean isActive) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void withdrawAction() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void ivanhoeAction() {
		// TODO Auto-generated method stub
		
	}
}
